<?php
namespace lce;

class ApiClient
{
    private $endpoint = 'http://www.bricozor.com/api/agoply';
    private $api_user_email;
    private $api_key;
    private $sandbox;

    /**
     * ApiClient constructor.
     * @param $api_user_email
     * @param $api_key
     * @param bool $sandbox
     * @param string|null $endpoint
     */
    public function __construct($api_user_email, $api_key, bool $sandbox = false, string $endpoint = null)
    {
        $this->api_user_email = $api_user_email;
        $this->api_key = $api_key;
        $this->sandbox = $sandbox;
        if($endpoint){
            $this->endpoint = $endpoint;
        }
    }

    /**
     * @return Order
     */
    public function createOrder()
    {
        return new Order($this->api_user_email);
    }

    /**
     * @param Order $order
     * @return int
     * @throws Exception
     */
    public function sendOrder(Order $order)
    {
        # sandbox mode, no actual request
        if($this->sandbox){return rand(0, 120000);}

        # real
        $json_string  = json_encode($order);
        $hash = hash_hmac('sha1', $json_string, $this->api_key);
        $response = \Requests::put($this->endpoint.'/order',['X-lce-hash' => $hash], $json_string);
        if($response->success){
            return $response->body;
        }else{
            $exception = new Exception('could not complete request', $response->status_code);
            $exception->setResponse($response);
            throw $exception;
        }
    }
}
