<?php
namespace lce;

class Exception extends \Exception{
    /**
     * @var \Requests_Response $response
     */
    private $response;
    public function setResponse(\Requests_Response $response){
        $this->response = $response;
    }

    /**
     * @return \Requests_Response
     */
    public function getResponse(){
        return $this->response;
    }
}