<?php
namespace lce;

/**
 * Class Order
 * @package lce
 */
class Order implements \JsonSerializable
{
    private $api_user_email;
    private $order_unique_identifier;
    private $items = [];
    private $contact = [
        'civil'        => 'M',
        'first_name'   => '',
        'last_name'    => '',
        'company'      => '',
        'email'        => '',
        'phone_number' => '',
        'adr_1'        => '',
        'adr_2'        => '',
        'zip'          => '',
        'city'         => '',
        'country_code' => 'FR'
    ];

    public function __construct($api_user_email)
    {
        $this->api_user_email = $api_user_email;
    }

    /**
     * @param string $article_code
     * @param int $qtt
     * @param float|null $unit_price
     * @return Order
     * @throws Exception
     */
    public function setItem(string $article_code, int $qtt = 1, float $unit_price = null): Order
    {
        if($qtt < 1){
            throw new Exception('wrong quantity');
        }
        $this->items[$article_code] = ['qtt' => $qtt, 'unit_price' => $unit_price];

        return $this;
    }

    /**
     * @param string $civil
     * @return Order
     * @throws Exception
     */
    public function setCivil(string $civil): Order
    {
        if(!in_array($civil, ['M','Mme', 'Mlle'])){
            throw new Exception('Must be one of M/Mme/Mlle');
        }
        $this->contact['civil'] = $civil;

        return $this;
    }

    /**
     * @param string $first_name
     * @return Order
     */
    public function setFirstName(string $first_name): Order
    {
        $this->contact['first_name'] = trim($first_name);

        return $this;
    }

    /**
     * @param string $last_name
     * @return Order
     */
    public function setLastName(string $last_name): Order
    {
        $this->contact['last_name'] = trim($last_name);

        return $this;
    }

    /**
     * @param string $company
     * @return Order
     */
    public function setCompany(string $company): Order
    {
        $this->contact['company'] = trim($company);

        return $this;
    }

    /**
     * @param string $email
     * @return Order
     * @throws Exception
     */
    public function setEmail(string $email): Order
    {
        if(!strstr($email, '@')){
            throw new Exception('not an email');
        }
        $this->contact['email'] = trim($email);

        return $this;
    }

    /**
     * @param string $phone_number
     * @return Order
     */
    public function setPhoneNumber(string $phone_number): Order
    {
        $this->contact['phone_number'] = trim($phone_number);

        return $this;
    }

    /**
     * @param string $address_line_1
     * @return Order
     */
    public function setAddressLine1(string $address_line_1): Order
    {
        $this->contact['adr_1'] = trim($address_line_1);

        return $this;
    }

    /**
     * @param string $address_line_2
     * @return Order
     */
    public function setAddressLine2(string $address_line_2): Order
    {
        $this->contact['adr_2'] = trim($address_line_2);

        return $this;
    }

    /**
     * @param string $zip
     * @return Order
     */
    public function setZip(string $zip): Order
    {
        $this->contact['zip'] = trim($zip);

        return $this;
    }

    /**
     * @param string $city
     * @return Order
     */
    public function setCity(string $city): Order
    {
        $this->contact['city'] = trim($city);

        return $this;
    }

    /**
     * @param string $country_code
     * @return Order
     */
    public function setCountryCode(string $country_code): Order
    {
        $this->contact['country_code'] = trim($country_code);

        return $this;
    }

    /**
     * @param string $order_unique_identifier
     * @return Order
     */
    public function setOrderUniqueIdentifier(string $order_unique_identifier): Order
    {
        $this->order_unique_identifier = $order_unique_identifier;

        return $this;
    }

    /**
     * @param float $fees
     * @return Order
     * @throws Exception
     */
    public function setTransportFees(float $fees): Order
    {
        $this->setItem('transport_fees', 1, $fees);

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'api_user_email'          => $this->api_user_email,
            'order_unique_identifier' => $this->order_unique_identifier,
            'items'                   => $this->items,
            'contact'                 => $this->contact
        ];
    }
}